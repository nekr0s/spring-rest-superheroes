package rusev.alexander.springrest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rusev.alexander.springrest.models.Hero;
import rusev.alexander.springrest.services.HeroService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/heroes")
public class HeroesController {

    private final HeroService heroService;

    @Autowired
    public HeroesController(HeroService heroService) {
        this.heroService = heroService;
    }

    @GetMapping
    public List<Hero> getHeroes() {
        return heroService.getHeroList();
    }

    @GetMapping("/{id}")
    public Hero getHeroById(@PathVariable String id) {
        return heroService.getHeroById(id);
    }

    @PostMapping
    public void createHero(@Valid @RequestBody Hero hero) {
        heroService.addHero(hero);
    }

    @PutMapping("/{id}")
    public void updateHero(@Valid @RequestBody Hero hero, @PathVariable String id) {
        heroService.updateHero(id, hero);
    }

    @DeleteMapping("/{id}")
    public void deleteHero(@PathVariable String id) {
        heroService.removeHero(id);
    }
}
