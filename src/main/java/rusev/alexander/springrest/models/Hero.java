package rusev.alexander.springrest.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Hero {
    private String id;
    @NotNull
    @Size(min = 2, message = "Name should have atleast 2 characters.")
    private String name, realName;
    @NotNull
    @Size(min = 3, message = "Story is way too short!")
    private String shortStory;

    public Hero() {
        // keep empty
    }

    public Hero(String name, String realName, String shortStory) {
        this.name = name;
        this.realName = realName;
        this.shortStory = shortStory;
        setId(name);
    }

    public String getId() {
        return id;
    }

    public void setId(String name) {
        this.id = name.toLowerCase().replace(' ', '+');
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getShortStory() {
        return shortStory;
    }

    public void setShortStory(String shortStory) {
        this.shortStory = shortStory;
    }
}
