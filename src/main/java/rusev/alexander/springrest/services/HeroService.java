package rusev.alexander.springrest.services;

import org.springframework.stereotype.Service;
import rusev.alexander.springrest.models.Hero;
import rusev.alexander.springrest.repository.HeroRepository;

import java.util.List;

@Service
public class HeroService {

    private HeroRepository repository;

    public HeroService() {
        repository = new HeroRepository();
    }

    public List<Hero> getHeroList() {
        return repository.getHeroList();
    }

    public Hero getHeroById(String id) {
        return repository.getHeroById(id);
    }

    public void addHero(Hero hero) {
        repository.addHero(hero);
    }

    public void updateHero(String id, Hero hero) {
        repository.updateHero(id, hero);
    }

    public void removeHero(String id) {
        repository.removeHero(id);
    }
}
