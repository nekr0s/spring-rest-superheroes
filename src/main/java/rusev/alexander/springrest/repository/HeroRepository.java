package rusev.alexander.springrest.repository;

import rusev.alexander.springrest.models.Hero;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Repository
public class HeroRepository implements Repository {

    private List<Hero> heroList = new ArrayList<>();

    public HeroRepository() {
        heroList.add(new Hero("Batman", "Bruce Wayne", "He is a bat!"));
        heroList.add(new Hero("Batgirl", "Barbara Gordon", "She got shot by The Joker :("));
        heroList.add(new Hero("Wonder Woman", "Don't know", "Has a nice whip."));
    }


    @Override
    public List<Hero> getHeroList() {
        return heroList;
    }

    @Override
    public Hero getHeroById(String id) {
        return heroList.stream()
                .filter(hero -> hero.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void addHero(Hero hero) {
        heroList.add(hero);
    }

    @Override
    public void updateHero(String id, Hero hero) {
        for (int i = 0; i < heroList.size(); i++)
            if (heroList.get(i).getId().equals(id)) {
                heroList.set(i, hero);
                return;
            }
    }

    @Override
    public void removeHero(String id) {
        heroList.removeIf(hero -> hero.getId().equals(id));
    }
}
