package rusev.alexander.springrest.repository;

import rusev.alexander.springrest.models.Hero;

import java.util.List;

public interface Repository {
    List<Hero> getHeroList();

    Hero getHeroById(String id);

    void addHero(Hero hero);

    void updateHero(String id, Hero hero);

    void removeHero(String id);
}
